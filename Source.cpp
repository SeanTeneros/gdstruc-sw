#include <iostream>
#include <string>
#include <conio.h>

using namespace std;
int sumOfnumber(int user) {

	if (user == 0)
		return 0;
	return (user % 10 + sumOfnumber(user / 10));
};
int Fibsequence(int n)
{
	if (n <= 1)
		return n;
	return (Fibsequence(n - 1) + Fibsequence(n - 2));

}
bool Primecheck(int input, int i = 2)
{

	if (input <= 2)
		return (input == 2) ? true : false;
	if (input % i == 0)
		return false;
	if (i * i > input)
		return true;
	return Primecheck(input, i + 1);
}
int main()
{
	//for #1
	int user;
	cout << "Enter the number of digits: " << endl;
	cin >> user;
	int output = sumOfnumber(user);
	cout << "The sum of " << user << " is " << output << endl;
	system("pause");


	//for #2
	int n;
	cout << "Enter the number of terms of series : ";
	cin >> n;
	cout << "Fibonnaci Series : " << Fibsequence(n) << endl;
	getchar();
	system("pause");

	//for #3
	int userinput;
	cout << "Enter a number to check if its a Prime number or not: " << endl;
	cin >> userinput;
	if (Primecheck(userinput))
		cout << "Yes! It's a Prime number";
	else
		cout << "No! It's not";

	system("pause");
	return 0;

}